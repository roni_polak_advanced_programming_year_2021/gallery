#pragma once
#include "IDataAccess.h"
#include "sqlite3.h"
#include "AlbumManager.h"
#include <io.h>

class DataAccessTest
{
public:
	//this function creates the users table
	void createUserTable();
	//this function creates the albums table
	void createAlbumTable();
	//this function creates the pictures table
	void createPictureTable();
	//this function creates the tags table
	void createTagTable();
	//this function opens the database
	void openDb();
	//this function closes the database
	void closeDb();
	//this function updates the database
	void update(std::string tableName, std::string column, std::string newName, std::string oldName);
private:
	sqlite3* _db;
};
