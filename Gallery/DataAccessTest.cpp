#include "DataAccessTest.h"


void DataAccessTest::createUserTable()
{
	//creates the user table
	std::string sqlStatement = "CREATE TABLE IF NOT EXISTS USERS (ID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , NAME TEXT NOT NULL);";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "failed to write queue" << std::endl;
}
void DataAccessTest::createAlbumTable()
{
	//creates the album table
	std::string sqlStatement = "CREATE TABLE IF NOT EXISTS ALBUMS(ID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, NAME TEXT NOT NULL, USER_ID INTEGER NOT NULL, CREATION_DATE TEXT NOT NULL, FOREIGN KEY(USER_ID) REFERENCES USERS(ID));";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "failed to write queue" << std::endl;
}
void DataAccessTest::createPictureTable()
{
	//creates the picture table
	std::string sqlStatement = "CREATE TABLE IF NOT EXISTS PICTURES ( ID  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,  NAME TEXT  NOT NULL, LOCATION TEXT NOT NULL,CREATION_DATE TEXT NOT NULL, ALBUM_ID INTEGER NOT NULL,  FOREIGN KEY(ALBUM_ID ) REFERENCES ALBUMS (ID));";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "failed to write queue" << std::endl;
}
void DataAccessTest::createTagTable()
{
	//creates the tag table
	std::string sqlStatement = "CREATE TABLE IF NOT EXISTS TAGS (PICTURE_ID INTEGER NOT NULL, USER_ID INTEGER NOT NULL, PRIMARY KEY(PICTURE_ID,USER_ID),  FOREIGN KEY(PICTURE_ID ) REFERENCES PICTURES (ID), FOREIGN KEY(USER_ID ) REFERENCES USERS (ID));";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "failed to write queue" << std::endl;
}

void DataAccessTest::openDb()
{
	//opens the database
	bool openSucceded = false;
	std::string dbFileName = "galleryDb.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &this->_db);
	if (res != SQLITE_OK) {
		_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
	}
	else
	{
		openSucceded = true;
	}
}

void DataAccessTest::update(std::string tableName, std::string column, std::string newName, std::string oldName)
{
	//updates a table
	std::string sqlStatement = "UPDATE ";
	sqlStatement += tableName;
	sqlStatement += " SET ";
	sqlStatement += column;
	sqlStatement += " = '";
	sqlStatement += oldName;
	sqlStatement += "' WHERE NAME = '";
	sqlStatement += newName;
	sqlStatement += "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "failed to write queue" << std::endl;
}

void DataAccessTest::closeDb()
{
	//closes the DB
	sqlite3_close(this->_db);
	_db = nullptr;
}