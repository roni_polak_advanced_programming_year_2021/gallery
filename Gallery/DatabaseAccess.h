#pragma once
#include "IDataAccess.h"
#include "sqlite3.h"
#include "AlbumManager.h"
#include <io.h>
class DatabaseAccess : public IDataAccess
{
public:

	~DatabaseAccess() = default;

	// album related


	//this function returns a list of albums
	const std::list<Album> getAlbums() override;
	//this function returns a list of albums of a user
	const std::list<Album> getAlbumsOfUser(const User& user) override;
	//this function creates a album
	void createAlbum(const Album& album) override;
	//this function checks if a album exists
	bool doesAlbumExists(const std::string& albumName, int userId) override;
	//this function opens a album
	Album openAlbum(const std::string& albumName) override;
	//this function prints all of the albums
	void printAlbums() override;
	//this function closes an album
	void closeAlbum(Album& pAlbum) override;
	//this function deletes an album
	void deleteAlbum(const std::string& albumName, int userId) override;

	// picture related


	//this function tags a user in a picture
	void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	//this function untags a user in picture
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	//this function adds a picture to an album by name
	void addPictureToAlbumByName(const std::string& albumName, const Picture& picture) override;
	//this function removes a picture from an album by name
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) override;

	// user related

	//this function prints all of the users
	void printUsers() override;
	//this function checks if a user exists
	bool doesUserExists(int userId) override;
	//this function gets a user
	User getUser(int userId) override;
	//this function creates a user
	void createUser(User& user) override;
	//this function deletes a user
	void deleteUser(const User& user) override;

	// user statistics

	//this function counts the number of albums owned by a user
	int countAlbumsOwnedOfUser(const User& user) override;
	//this function counts the number of albums that are tagged of a user
	int countAlbumsTaggedOfUser(const User& user) override;
	//this function counts how many times a user was 
	int countTagsOfUser(const User& user) override;
	//count the average tags a user has in an album
	float averageTagsPerAlbumOfUser(const User& user) override;
	
	// queries

	//this function returns the top tagged user
	User getTopTaggedUser() override;
	//this function returns the top tagged picture
	Picture getTopTaggedPicture() override;
	//this function returns the top tagged picture of a user
	std::list<Picture> getTaggedPicturesOfUser(const User& user) override;
	//this function opens the database
	bool open() override;
	//this function closes the database
	void close() override;
	//this function clears all the data saved in the class
	void clear() override;



private:
	
	sqlite3* _db;

};