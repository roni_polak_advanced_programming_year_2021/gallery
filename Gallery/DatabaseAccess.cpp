#include "DatabaseAccess.h"


int callbackAverageTags(void* data, int argc, char** argv, char** azColName);
int callbackGetUser (void* data, int argc, char** argv, char** azColName);
int callbackPicture(void* data, int argc, char** argv, char** azColName);
int callbackTaggedPictures(void* data, int argc, char** argv, char** azColName);
int callbackCount(void* data, int argc, char** argv, char** azColName);
int callbackPrint(void* data, int argc, char** argv, char** azColName);
int checkIfExists(void* data, int argc, char** argv, char** azColName);
int callBackGetAlbum(void* data, int argc, char** argv, char** azColName);
int callBackopenAlbum(void* data, int argc, char** argv, char** azColName);
int checkIfAlbumExists(void* data, int argc, char** argv, char** azColName);
int callbackTags(void* data, int argc, char** argv, char** azColName);

void DatabaseAccess::closeAlbum(Album& pAlbum)
{
	
}

bool DatabaseAccess::open()
{
	bool openSucceded = false;
	std::string dbFileName = "galleryDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &this->_db);
	if (res != SQLITE_OK) //if failed to open
	{
		_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
	}
	else
	{
		openSucceded = true;
	}
	return openSucceded;
}
void DatabaseAccess::close()
{
	sqlite3_close(this->_db);//closing the DB
	_db = nullptr;
}
void DatabaseAccess::clear()
{
	
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{

	//deleting all tags from pictures of user
	std::string sqlStatement = "DELETE FROM TAGS WHERE PICTURE_ID in(SELECT ID FROM PICTURES WHERE ALBUM_ID in (SELECT ID FROM ALBUMS WHERE NAME = '";
	sqlStatement += albumName;
	sqlStatement += "' AND USER_ID =";
	sqlStatement += std::to_string(userId);
	sqlStatement += "));";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	//deleting all pictures from album of user
	sqlStatement = "DELETE FROM PICTURES WHERE ALBUM_ID in (SELECT ID FROM ALBUMS WHERE NAME = '";
	sqlStatement += albumName;
	sqlStatement += "' AND USER_ID =";
	sqlStatement += std::to_string(userId);
	sqlStatement += ");";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	//deleting all albums of user
	sqlStatement = "DELETE FROM ALBUMS WHERE ID in (SELECT ID FROM ALBUMS WHERE NAME = '";
	sqlStatement += albumName;
	sqlStatement += "' AND USER_ID =";
	sqlStatement += std::to_string(userId);
	sqlStatement += ");";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
}



void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	//inserting a tag into a picture
	std::string sqlStatement = "INSERT INTO TAGS (USER_ID, PICTURE_ID) VALUES(";
	sqlStatement += std::to_string(userId);
	sqlStatement += ", (SELECT ID FROM PICTURES WHERE NAME = '";
	sqlStatement += pictureName;
	sqlStatement += "' AND ALBUM_ID in(SELECT ID FROM ALBUMS WHERE NAME = '";
	sqlStatement += albumName;
	sqlStatement += "')));";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
}
void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	//removing a tag from a picture
	std::string sqlStatement = "DELETE FROM TAGS WHERE USER_ID = ";
	sqlStatement += std::to_string(userId);
	sqlStatement += " AND PICTURE_ID in (SELECT ID FROM PICTURES WHERE NAME = '";
	sqlStatement += pictureName;
	sqlStatement += "' AND ALBUM_ID in (SELECT ID FROM ALBUMS WHERE NAME = '";
	sqlStatement += albumName;
	sqlStatement += "'));";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
}

void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	//adding a picture to an album
	std::string sqlStatement = "INSERT INTO PICTURES (NAME, LOCATION, ALBUM_ID, CREATION_DATE) VALUES ('";
	sqlStatement += picture.getName();
	sqlStatement += "', '";
	sqlStatement += picture.getPath();
	sqlStatement += "', (SELECT ID FROM ALBUMS WHERE NAME = '";
	sqlStatement += albumName;
	sqlStatement += "'), '";
	sqlStatement += picture.getCreationDate();
	sqlStatement += "');";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
}
void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	//deleting all tags in a picture
	std::string sqlStatement = "DELETE FROM TAGS WHERE PICTURE_ID in(SELECT ID FROM PICTURES WHERE ALBUM_ID in (SELECT ID FROM ALBUMS WHERE NAME =  '";
	sqlStatement += albumName;
	sqlStatement += "') AND NAME = '";
	sqlStatement += pictureName;
	sqlStatement += "');";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	//deleting the picture from the album
	sqlStatement = "DELETE FROM PICTURES WHERE ALBUM_ID in(SELECT ID FROM ALBUMS WHERE NAME =  '";
	sqlStatement += albumName;
	sqlStatement += "') AND NAME = '";
	sqlStatement += pictureName;
	sqlStatement += "';";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
}

void DatabaseAccess::createUser(User& user)
{
	//inserts a new user to the DB
	std::string sqlStatement = "INSERT INTO USERS (NAME) VALUES ('";
	sqlStatement += user.getName();
	sqlStatement += "');";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
}
void DatabaseAccess::deleteUser(const User& user)
{
	//deletes all tags of user
	std::string sqlStatement = "DELETE FROM TAGS WHERE PICTURE_ID in(SELECT ID FROM PICTURES WHERE ALBUM_ID in (SELECT ID FROM ALBUMS WHERE USER_ID =";
	sqlStatement += std::to_string(user.getId());
	sqlStatement += "));";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	//deletes all pictures of user
	sqlStatement = "DELETE FROM PICTURES WHERE ALBUM_ID in (SELECT ID FROM ALBUMS WHERE USER_ID =";
	sqlStatement += std::to_string(user.getId());
	sqlStatement += ");";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	//deletes all albums of user
	sqlStatement = "DELETE FROM ALBUMS WHERE USER_ID =";
	sqlStatement += std::to_string(user.getId());
	sqlStatement += ";";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	//deletes all tags where user was tagged
	sqlStatement = "DELETE FROM TAGS WHERE USER_ID =";
	sqlStatement += std::to_string(user.getId());
	sqlStatement += ";";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	//delete user from DB
	sqlStatement = "DELETE FROM USERS WHERE ID =";
	sqlStatement += std::to_string(user.getId());
	sqlStatement += ";";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	//calculate average tags per album of a user
	std::string sqlStatement = "SELECT (SELECT count(*) FROM TAGS WHERE PICTURE_ID in (SELECT ID FROM PICTURES WHERE ALBUM_ID in (SELECT ID FROM ALBUMS WHERE USER_ID =";
	sqlStatement += std::to_string(user.getId());
	sqlStatement += "))) /  CAST((SELECT count(*) FROM ALBUMS WHERE USER_ID =";
	sqlStatement += std::to_string(user.getId());
	sqlStatement += ") AS FLOA);";
	char* errMessage = nullptr;
	float* data = new float;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callbackAverageTags , data, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;

	return *data;
}

User DatabaseAccess::getTopTaggedUser()
{
	//get the top tagged user
	std::string sqlStatement = "SELECT * FROM USERS WHERE ID IN (SELECT USER_ID FROM TAGS GROUP BY USER_ID HAVING COUNT(*) = (SELECT  MAX(Cnt) FROM (SELECT COUNT(*) AS Cnt FROM TAGS GROUP BY USER_ID) tmp));";
	char* errMessage = nullptr;
	User data(0, "");
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callbackGetUser, &data, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;

	return data;
}
Picture DatabaseAccess::getTopTaggedPicture()
{
	//get the top tagged picture
	std::string sqlStatement = "SELECT * FROM PICTURES WHERE ID IN (SELECT PICTURE_ID FROM TAGS GROUP BY PICTURE_ID HAVING COUNT(*) = (SELECT  MAX(Cnt) FROM (SELECT COUNT(*) AS Cnt FROM TAGS GROUP BY PICTURE_ID) tmp)) LIMIT 1;";
	char* errMessage = nullptr;
	Picture data(0, "");
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callbackPicture, &data, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	//get the tags from the picture
	sqlStatement = "SELECT USER_ID FROM TAGS WHERE PICTURE_ID = ";
	sqlStatement += std::to_string(data.getId());
	sqlStatement += ";";
	errMessage = nullptr;
	std::list<int> p;
	res = sqlite3_exec(_db, sqlStatement.c_str(), callbackTags, &p, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	for (auto it = p.begin(); it != p.end(); ++it)//add the tags to the picture
	{
		data.tagUser(*it);
	}
	return data;
}
std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	//get the tagged pictures of a user
	std::string sqlStatement = "SELECT * FROM PICTURES WHERE ID IN (SELECT PICTURE_ID FROM TAGS WHERE USER_ID  = ";
	sqlStatement += std::to_string(user.getId());
	sqlStatement += ");";
	char* errMessage = nullptr;
	std::list<Picture> p;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callbackTaggedPictures, &p, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;

	for (auto it = p.begin(); it != p.end(); ++it)//for the list of pictures
	{
		sqlStatement = "SELECT USER_ID FROM TAGS WHERE PICTURE_ID = ";
		sqlStatement += std::to_string(it->getId());
		sqlStatement += ";";
		errMessage = nullptr;
		std::list<int> tagsList;
		res = sqlite3_exec(_db, sqlStatement.c_str(), callbackTags, &tagsList, &errMessage);
		if (res != SQLITE_OK)
			std::cout << "Failed to write queue" << std::endl;
		for (auto itTag = tagsList.begin(); itTag != tagsList.end(); ++itTag)//add all tags of each picture into it
		{
			it->tagUser(*itTag);
		}
	}

	return p;
}

int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	//count the albums owned by a user
	std::string sqlStatement = "SELECT COUNT(*) FROM ALBUMS WHERE USER_ID = ";
	sqlStatement += std::to_string(user.getId());
	sqlStatement += ";";
	char* errMessage = nullptr;
	int data;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callbackCount, &data, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;

	return data;
}
int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	//count the number of albums that have tags of a user
	std::string sqlStatement = "SELECT COUNT(distinct ALBUM_ID) FROM PICTURES WHERE ID in (SELECT PICTURE_ID FROM TAGS WHERE USER_ID  = ";
	sqlStatement += std::to_string(user.getId());
	sqlStatement += ");";
	char* errMessage = nullptr;
	int data;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callbackCount, &data, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;

	return data;
}
int DatabaseAccess::countTagsOfUser(const User& user)
{
	//count how many tags a user has
	std::string sqlStatement = "SELECT COUNT(PICTURE_ID) FROM TAGS WHERE USER_ID  = ";
	sqlStatement += std::to_string(user.getId());
	sqlStatement += ";";
	char* errMessage = nullptr;
	int data;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callbackCount, &data, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;

	return data;
}

void DatabaseAccess::printUsers()
{
	//print users
	std::string sqlStatement = "SELECT * FROM USERS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callbackPrint, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
}


User DatabaseAccess::getUser(int userId)
{
	//return a user
	std::string sqlStatement = "SELECT * FROM USERS WHERE ID = ";
	sqlStatement += std::to_string(userId);
	sqlStatement += ";";
	char* errMessage = nullptr;
	User data(0, "");
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callbackGetUser, &data, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;

	return data;
}

const std::list<Album> DatabaseAccess::getAlbums()
{
	//returns the list of albums
	std::string sqlStatement = "SELECT * FROM ALBUMS;";
	char* errMessage = nullptr;
	std::list<Album> data;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callBackGetAlbum, &data, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;

	return data;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	//return a list of albums of a user
	std::string sqlStatement = "SELECT * FROM ALBUMS WHERE USER_ID = ";
	sqlStatement += std::to_string(user.getId());
	char* errMessage = nullptr;
	std::list<Album> data;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callBackGetAlbum, &data, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;

	return data;
}

void DatabaseAccess::createAlbum(const Album& album)
{
	//creates an album
	std::string sqlStatement = "INSERT INTO ALBUMS (NAME, USER_ID, CREATION_DATE) VALUES ('";
	sqlStatement += album.getName();
	sqlStatement += "', ";
	sqlStatement += std::to_string(album.getOwnerId());
	sqlStatement += ", '";
	sqlStatement += album.getCreationDate();
	sqlStatement += "');";
	char* errMessage = nullptr;

	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	//checks if album exists
	std::string sqlStatement = "SELECT COUNT(*) FROM ALBUMS WHERE NAME = '";
	sqlStatement += albumName;
	sqlStatement += "' AND USER_ID = ";
	sqlStatement += std::to_string(userId);
	sqlStatement += ";";
	char* errMessage = nullptr;
	int data;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), checkIfAlbumExists, &data, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	if (data == 0)//if count is 0 then there are no albums
	{
		return false;
	}
	else//there is at least one album
	{
		return true;
	}
}

bool DatabaseAccess::doesUserExists(int userId)
{
	std::string sqlStatement = "SELECT COUNT(*) FROM USERS WHERE ID = ";
	sqlStatement += std::to_string(userId);
	sqlStatement += ";";
	char* errMessage = nullptr;
	int data;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), checkIfExists, &data, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	if (data > 0)
	{
		//there is at least one album
		return true;
	}
	else
	{
		//there are no albums
		return false;
	}
}
Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	//gets an album
	std::string sqlStatement = "SELECT * FROM ALBUMS WHERE NAME = '";
	sqlStatement += albumName;
	sqlStatement += "';";
	char* errMessage = nullptr;
	Album data(5, "A");
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callBackopenAlbum, &data, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	//adds all of the pictures to the album 
	sqlStatement = "SELECT * FROM PICTURES WHERE ALBUM_ID IN (SELECT ID FROM ALBUMS WHERE NAME = '";
	sqlStatement += albumName;
	sqlStatement += "');";
	errMessage = nullptr;
	std::list<Picture> p;
	res = sqlite3_exec(_db, sqlStatement.c_str(), callbackTaggedPictures, &p, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;
	
	//adds all of the tags to the picture
	for (auto itPicture = p.begin(); itPicture != p.end(); ++itPicture)
	{
		sqlStatement = "SELECT USER_ID FROM TAGS WHERE PICTURE_ID = ";
		sqlStatement += std::to_string(itPicture->getId());
		sqlStatement += ";";
		errMessage = nullptr;
		std::list<int> tagsList;
		res = sqlite3_exec(_db, sqlStatement.c_str(), callbackTags, &tagsList, &errMessage);
		if (res != SQLITE_OK)
			std::cout << "Failed to write queue" << std::endl;
		for (auto itTag = tagsList.begin(); itTag != tagsList.end(); ++itTag)
		{
			itPicture->tagUser(*itTag);
		}
	}
	for (auto it = p.begin(); it != p.end(); ++it)
	{
		data.addPicture(*it);
	}
	return data;
}

void DatabaseAccess::printAlbums()
{
	//prints all of the albums
	std::string sqlStatement = "SELECT * FROM ALBUMS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callbackPrint, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failed to write queue" << std::endl;

}



int callbackAverageTags(void* data, int argc, char** argv, char** azColName)
{
	float* avgTags = (float*)data;
	if (argv[0] != NULL)
	{
		for (int i = 0; i < argc; i++)
		{
			*avgTags = std::stof(argv[i]);
		}
	}
	else
	{
		*avgTags = 0.0;
	}
	
	return 0;
}

int callbackGetUser(void* data, int argc, char** argv, char** azColName)
{
	//sets the users data
	User* user = (User*)data;
	user->setId(std::stoi(argv[0]));
	user->setName(argv[1]);
	return 0;
}

int callbackPicture(void* data, int argc, char** argv, char** azColName)
{
	//sets the picture data
	Picture* picture = (Picture*)data;
	picture->setId(std::stoi(argv[0]));
	picture->setName(argv[1]);
	picture->setPath(argv[2]);
	picture->setCreationDate(argv[3]);
	return 0;
}

int callbackTaggedPictures(void* data, int argc, char** argv, char** azColName)
{
	std::list<Picture>* p = (std::list<Picture>*)data;
	Picture picture(1, "c");
	//sets the picture data
	picture.setId(std::stoi(argv[0]));
	picture.setName(argv[1]);
	picture.setPath(argv[2]);
	picture.setCreationDate(argv[3]);
	p->push_back(picture);//pushes picture into list
	return 0;
}


int callbackTags(void* data, int argc, char** argv, char** azColName)
{
	std::list<int>* p = (std::list<int>*)data;
	p->push_back(atoi(argv[0]));//pushes tag into list
	return 0;
}

int callbackCount(void* data, int argc, char** argv, char** azColName)
{
	//sets count
	int* count = (int*)data;

	*count = std::stoi(argv[0]);

	return 0;
}

int callbackPrint(void* data, int argc, char** argv, char** azColName)
{
	//prints the data of a table
	for (int i = 0; i < argc; i++)
	{
		std::cout << azColName[i] << " = " << argv[i] << " , ";
	}
	std::cout << std::endl;

	return 0;
}

int checkIfExists(void* data, int argc, char** argv, char** azColName)
{
	//checks if entity exists
	int* checker = (int*) data;
	*checker = atoi(argv[0]);
	return 0;
}

int checkIfAlbumExists(void* data, int argc, char** argv, char** azColName)
{
	//checks if album exists
	int* checker = (int*)data;
	*checker = atoi(argv[0]);
	return 0;
}

int callBackGetAlbum(void* data, int argc, char** argv, char** azColName)
{
	//sets the album data
	std::list<Album>* p = (std::list<Album>*)data;
	Album album(1, "c");
	album.setName(argv[1]);
	album.setOwner(atoi(argv[2]));
	album.setCreationDate(argv[3]);
	p->push_back(album);//pushes album into list

	return 0;
}

int callBackopenAlbum(void* data, int argc, char** argv, char** azColName)
{
	//sets the album data
	Album* album = (Album*)data;
	album->setName(argv[1]);
	album->setOwner(atoi(argv[2]));
	album->setCreationDate(argv[3]);

	return 0;
}