#include <iostream>
#include <string>
#include <ctime>
#include "MemoryAccess.h"
#include "AlbumManager.h"
#include "DataAccessTest.h"
#include "DatabaseAccess.h"


int getCommandNumberFromUser()
{


	std::string message("\nPlease enter any command(use number): ");
	std::string numericStr("0123456789");
	
	std::cout << message << std::endl;
	std::string input;
	std::getline(std::cin, input);
	
	while (std::cin.fail() || std::cin.eof() || input.find_first_not_of(numericStr) != std::string::npos) {

		std::cout << "Please enter a number only!" << std::endl;

		if (input.find_first_not_of(numericStr) == std::string::npos) {
			std::cin.clear();
		}

		std::cout << std::endl << message << std::endl;
		std::getline(std::cin, input);
	}
	
	return std::atoi(input.c_str());
}

void printDetails()
{
	time_t now = time(0);
	char* dt = ctime(&now);
	std::cout << "Roni Polak " << dt;
}

int main(void)
{

	DataAccessTest tester;

	tester.openDb();
	tester.createAlbumTable();
	tester.createPictureTable();
	tester.createTagTable();
	tester.createUserTable();


	//tester.update("PICTURES", "NAME", "My Femily", "My Family");
	tester.closeDb();
	// initialization data access
	//MemoryAccess dataAccess;
	DatabaseAccess dataAccess;
	// initialize album manager
	AlbumManager albumManager(dataAccess);


	std::string albumName;
	printDetails();
	std::cout << "Welcome to Gallery!" << std::endl;
	std::cout << "====================" << std::endl;
	std::cout << "Type " << HELP << " to a list of all supported commands" << std::endl;
	
	do {
		int commandNumber = getCommandNumberFromUser();
		
		try	{
			albumManager.executeCommand(static_cast<CommandType>(commandNumber));
		} catch (std::exception& e) {	
			std::cout << e.what() << std::endl;
		}
	} 
	while (true);
}


